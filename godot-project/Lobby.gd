#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends Control

onready var MAIN = get_node("/root/Main")

var settings = {
				"playerName":	"",
				"serverIp":		"127.0.0.1"
}

func _ready():
	self.loadSettings()
	
	var loadedName = self.getSetting("playerName")
	if (loadedName != null):
		$Player/InputName.text = loadedName
		self._on_InputName_text_changed(loadedName)
	var loadedIp = self.getSetting("serverIp")
	if (loadedIp != null):
		$Join/InputIp.text = loadedIp

func _on_ButtonHost_pressed():
	MAIN.playSoundMenuButtonClick()
	
	var playerName = $Player/InputName.text
	self.setSetting("playerName", playerName)
	
	ConnectionManager.on_host_game(playerName)

func _on_ButtonJoin_pressed():
	MAIN.playSoundMenuButtonClick()
	
	var ip = $Join/InputIp.text
	self.setSetting("serverIp", ip)
	var playerName = $Player/InputName.text
	self.setSetting("playerName", playerName)
	
	ConnectionManager.on_join_game(ip, playerName)

func _on_ButtonStart_pressed():
	MAIN.playSoundMenuButtonClick()
	
	MAIN.rpc("initGame", ConnectionManager.players, randi())
	MAIN.rpc("startGame")

func _on_InputName_text_changed(new_text):
	get_node("Host/ButtonHost").disabled = false
	get_node("Join/ButtonJoin").disabled = false

func setSetting(name, value):
	self.settings[name] = value
	self.saveSettings()

func getSetting(name):
	return self.settings[name]

func saveSettings():
	var settingsFile = ConfigFile.new()
	for setting in settings:
		settingsFile.set_value("PlayerSettings", setting, settings[setting])
	settingsFile.save(MAIN.settingsFilePath)

func loadSettings():
	var settingsFile = ConfigFile.new()
	settingsFile.load(MAIN.settingsFilePath)
	for setting in settings:
		settings[setting] = settingsFile.get_value("PlayerSettings", setting)
