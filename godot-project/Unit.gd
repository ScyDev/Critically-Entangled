#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends RigidBody

onready var MAIN = get_node("/root/Main")

var effectScene = preload("res://Effect.tscn")

const WEAPON_TYPE_PROJECTILE = 1
const WEAPON_TYPE_HITSCAN = 2

var gameObjectType = "UNIT"
var weaponType = null
var projectileScene = null
var weaponName = null

var maxHealth = 100
var health = 100
var unitMovementSpeed = 20
var rotSpeed = 2

var tier = 1.0
var adjustedTier = 1.0 # used for scaling everything. a fraction of the int tier, because we don't want to scale in steps of 100%

var projectileDirectionSpawnOffset = 3.5
var projectileSpawnOffset = Vector3(0, 1.0, 0)

var destinationPoint = null

var weaponFiring = false
var weaponCooldown = 1.0
var weaponCooldownTimer = null
var weaponReady = true
var weaponMinRange = 0
var weaponMaxRange = 0

var destroyed = false

slave var slave_translation = Vector3()
slave var slave_rotation = Vector3()
slave var slave_health = 0
slave var slave_destinationPoint = null

var audioPlayers = {} # managing a dictionary of AudioPlayers because Godot 3.x doesn't have a polyphonic sound sampler anymore wtf...
var audioVolume = 30

func _ready():
	self.set_contact_monitor(true)
	self.set_max_contacts_reported(1000) # needed for collisions

	self.set_angular_damp(0.999)
	self.set_linear_damp(0.99)
	
	# make sure slave pos and rot are the same as the initial pos and rot set during instancing
	self.slave_translation = self.translation
	self.slave_rotation = self.rotation
	self.slave_health = self.health
	
	self.connect("body_entered", self, "_on_Unit_body_entered") # for collision damage
	
	self.weaponCooldownTimer = Timer.new()
	self.weaponCooldownTimer.connect("timeout", self, "_on_weaponCooldownTimer_timeout")
	self.weaponCooldownTimer.set_one_shot(true)
	self.add_child(weaponCooldownTimer)
	
	var netSyncTimer = Timer.new()
	netSyncTimer.connect("timeout", self, "_on_netSyncTimer_timeout")
	netSyncTimer.wait_time = 0.1 # 0.1 seems to be pretty close to the limit for a real netwoprk. at 0.05 the game freezed again. try again maybe.
	self.add_child(netSyncTimer)
	netSyncTimer.start()

func _physics_process(delta):
#	if is_network_master():
		# autonomous movement
	if (self.destinationPoint != null):
		if (self.destinationPoint - self.translation).length() >= 0.2:
#			print("_physics_process(delta) distance: ",((self.destinationPoint - self.translation).length()))
			var direction = self.destinationPoint - self.translation
#				self.look_at(self.destinationPoint, Vector3(0, 1, 0))
			self.apply_impulse(Vector3(0, 0, 0), direction.normalized() * self.unitMovementSpeed * 0.005)
		else:
			self.destinationPoint = null
	
	if (is_network_master() == false):
		self.updateUnit() # if called from here, enemy unit movement is steady with the tick of the sync timer, no freezes
	
	if (self.weaponReady != true):
		self.updateCooldownBar()

func _integrate_forces(state):
#	if is_network_master():
		# autonomous movement
		# if we only do this here in _integrate_forces, can't move my own units if they are not already being moved by the physics engine. damn.
		if (self.destinationPoint != null):
			if (self.destinationPoint - self.translation).length() >= 0.2:
#				print("_integrate_forces(state) distance: ",((self.destinationPoint - self.translation).length()))
				var direction = self.destinationPoint - self.translation
				self.look_at(self.destinationPoint, Vector3(0, 1, 0))
#				self.apply_impulse(Vector3(0, 0, 0), direction.normalized() * self.unitMovementSpeed * 0.005)
			else:
				self.destinationPoint = null
				#print("arrived!")
#	else:
#		self.updateUnit() # if called from here, we get lag spikes and short freezes of enemy unit movement, then correction

var readyToUpdate = true # to prevent updating every frame. only on each netSyncTimer tick.
func _on_netSyncTimer_timeout():
	self.syncUnit()
#	self.updateUnit()
	self.readyToUpdate = true

func syncUnit():
	if (is_network_master()):
#		rset_unreliable("slave_translation", self.translation)
#		rset_unreliable("slave_rotation", self.rotation)
		rset_unreliable("slave_health", self.health)
#		print("syncUnit ",self.get_name()," ",self.health)

func updateUnit():
	if (is_network_master() == false):
#		if (self.readyToUpdate == true): # this if causes lots of spastic movement when called from _physics_process(). and the big reverting movement freezes when called from _integrate_forces()?
#			self.translation = self.slave_translation
#			self.rotation = self.slave_rotation
			self.destinationPoint = self.slave_destinationPoint
#			print("updateUnit() self.slave_destinationPoint ",self.slave_destinationPoint)
			self.health = self.slave_health
			self.updateHealthBar()
	#		print("updateUnit ",self.get_name()," ",self.slave_health)
			self.readyToUpdate = false

func _on_Tank_input_event(camera, event, click_position, click_normal, shape_idx):
	get_node("/root/Main").handleInputEvent(self, camera, event, click_position, click_normal, shape_idx)

# apply player material to new unit
func setMaterial(playerMaterial):
	for child in self.get_node("ScaleWrapper/Model").get_children():
		if child.is_class("MeshInstance"):
			child.set_material_override(playerMaterial)

# RigidBody (the root) can't be scaled directly, it's scale is reset to (1, 1, 1) by the physics engine.
# So we scale CollisionShape and everything else in the ScaleWrapper
func adjustToTier(tier):
	adjustedTier = 1.0 + (tier * MAIN.tierGrowthMultiplier) # adjust scale here so we can use an integer as tier
	var adjustedScale = Vector3(1, 1, 1) * adjustedTier
	
	print("set scale to: "+str(adjustedScale))
	var collisionShapeMultiplier = 0.8
	self.get_node("CollisionShape").set_scale(adjustedScale * collisionShapeMultiplier) # multiplier necessary because Godot doesn't scale CollisionShapes at the same rate as other nodes... man
	self.get_node("ScaleWrapper").set_scale(adjustedScale) 
	
	self.maxHealth = self.maxHealth * adjustedTier
	self.health = self.maxHealth
	self.weaponMinRange = self.weaponMinRange * adjustedTier
	self.weaponMaxRange = self.weaponMaxRange * adjustedTier

func markAsSelected(mark):
	if (mark == true):
		if (self.get_node("SelectionMarker") == null):
			var sprite = get_node("/root/Main/Utils/SelectionMarker").duplicate()

			# scale the selection marker to unit size
			var unitSize = $CollisionShape.shape.height
			sprite.scale = sprite.scale * unitSize

			sprite.show()
			self.add_child(sprite)
	else:
		if self.get_node("SelectionMarker") != null:
			self.get_node("SelectionMarker").queue_free()

func setDestinationPoint(newDestinationPoint):
	newDestinationPoint.y = self.translation.y
	self.destinationPoint = newDestinationPoint
	
	if (is_network_master()):
		rset_unreliable("slave_destinationPoint", self.destinationPoint)
#		print("setDestinationPoint(newDestinationPoint) self.destinationPoint ",self.destinationPoint)

func prepareWeapon(player_id, targetPos = null):
	if (self.weaponReady == true):
		self.weaponReady = false
		self.startWeaponCooldownTimer()
		
		# this should be != null only during test
		var mouseTargetPos = targetPos
		if (mouseTargetPos == null):
			mouseTargetPos = MAIN.getMouseTargetPoint()
		
		self.rpc("fireWeapon", player_id, mouseTargetPos)
#	else:
#		MAIN.playSoundDenyClick()

sync func fireWeapon(player_id, mouseTargetPos):
	self.setDestinationPoint(self.translation) # make unit stop
	if (mouseTargetPos != null):
		self.lookAtTarget(mouseTargetPos)
	
	var newProjectile = projectileScene.instance()
	newProjectile.set_network_master(player_id)
	newProjectile.spawnedBy = self
	newProjectile.maxRange =  self.weaponMaxRange
	newProjectile.adjustedTier = self.adjustedTier
	
	newProjectile.rotation = self.rotation
	var direction = Vector3(sin(newProjectile.rotation.y), 0, cos(newProjectile.rotation.y)).normalized() * -1
	newProjectile.translation = self.translation + (direction * projectileDirectionSpawnOffset * self.adjustedTier) + (projectileSpawnOffset * self.adjustedTier)
	
	get_node("/root/Main/Projectiles").add_child(newProjectile)
	newProjectile.startMoving(direction)
	
	self.playSound(self.weaponName+"_fire")

func lookAtTarget(mouseTargetPos):
	self.look_at(mouseTargetPos, Vector3(0, 1, 0))
	
	# keep only rotation of Y axis, because look_at() changes all 3 axis
	var rot = self.get_rotation()
	rot.x = 0
	rot.z = 0
	self.set_rotation(rot)

sync func stopFiring(player_id):
	pass

func _on_weaponCooldownTimer_timeout():
	self.weaponReady = true
	get_node("ScaleWrapper/CooldownBar").hide()

func startWeaponCooldownTimer():
	self.weaponCooldownTimer.wait_time = self.weaponCooldown
	self.weaponCooldownTimer.start()
	get_node("ScaleWrapper/CooldownBar").show()

sync func takeDamage(damage):
	if (self.is_network_master()):
		self.health -= damage
		
		if (self.health < 0):
			self.health = 0
		
		rset_unreliable("slave_health", self.health)
		self.updateHealthBar()
		
		MAIN.setLastDamagedUnit(self)
		
		if (self.health <= 0):
			self.rpc("destroyUnit")

func updateHealthBar():
	var healthBar = get_node("ScaleWrapper/HealthBar")
	var healthWidth = (512.0 / self.maxHealth) * self.health
	healthBar.region_rect = Rect2(Vector2(0, 0), Vector2(healthWidth, 64))
	healthBar.offset = Vector2(-256, 0)
	var healthBarBg = get_node("ScaleWrapper/HealthBar/HealthBarBackground")
	var bgWidth = (512.0 / self.maxHealth) * (self.maxHealth - self.health)
	healthBarBg.region_rect = Rect2(Vector2(0, 0), Vector2(bgWidth, 64))
	healthBarBg.offset = Vector2(-256 + healthWidth, 0)

func updateCooldownBar():
	var timeElapsed = self.weaponCooldown - self.weaponCooldownTimer.get_time_left()
	
	var cooldownBar = get_node("ScaleWrapper/CooldownBar")
	var cooldownWidth = (512.0 / self.weaponCooldown) * timeElapsed
	cooldownBar.region_rect = Rect2(Vector2(0, 0), Vector2(cooldownWidth, 32))
	cooldownBar.offset = Vector2(-256, 0)
	var cooldownBarBg = get_node("ScaleWrapper/CooldownBar/CooldownBarBackground")
	var bgWidth = (512.0 / self.weaponCooldown) * (self.weaponCooldown - timeElapsed)
	cooldownBarBg.region_rect = Rect2(Vector2(0, 0), Vector2(bgWidth, 32))
	cooldownBarBg.offset = Vector2(-256 + cooldownWidth, 0)

sync func destroyUnit():
	if (!self.destroyed): # to prevent multiple calls of this function on the same unit. a unit should only be destroyed once.
		self.destroyed = true
		
		if (self.is_network_master()):
			MAIN.get_node("Sound/Voice/UnitLost").play()
		
		var newEffect = effectScene.instance()
		newEffect.set_translation(self.get_translation())
		get_node("/root/Main/Effects").add_child(newEffect)
		newEffect.playSound("explosion")
		newEffect.showParticleEffect("unit_explosion")
		
		self.queue_free()

func playSound(sampleName, loop = false):
	var audioPlayer = null
	# prepare a player for this sample. Godot 3.x doesn't have a polyphonic sample player anymore, so we have to do this manually.
	if (self.audioPlayers.get(sampleName) == null):
		print("new player for: ",sampleName)
		audioPlayer = AudioStreamPlayer3D.new()
		audioPlayer.set_unit_db(self.audioVolume)
		self.audioPlayers[sampleName] = audioPlayer
		self.add_child(audioPlayer)
	else:
		print("existing player for: ",sampleName)
		audioPlayer = self.audioPlayers[sampleName]
	
	var audioStream = load("res://assets/sounds/"+sampleName+".wav")
	
	# for looping samples
	if (loop == true):
		print("loop config for: ",sampleName)
		audioStream.set_loop_begin(0)
		audioStream.set_loop_end(audioStream.get_length() * audioStream.get_mix_rate()) # length (in seconds) * mix_rate = all bytes of the stream
		audioStream.set_loop_mode(AudioStreamSample.LOOP_FORWARD)
	
	# start playing a sound only if it is not playing already or if it is not a looping sound
	if (loop == false || audioPlayer.playing == false):
		print("play: ",sampleName," loop:",loop)
		audioPlayer.set_stream(audioStream) # this restarts playback if player is already playing a sample, so we don't wanna unnecessarilly call it.
		audioPlayer.play()
	else:
		print("don't play")

func stopSound(sampleName):
	if (self.audioPlayers.get(sampleName) != null):
		print("stopping sound: ",sampleName)
		self.audioPlayers[sampleName].stop()

# unit collisions
func _on_Unit_body_entered(body):
	var bodyGameObjectType = body.get("gameObjectType")
	if (bodyGameObjectType != null): 
		# units only damage units of another player
		if (bodyGameObjectType == "UNIT" && body.get_network_master() != self.get_network_master()):
			body.takeDamage(MAIN.collisionDamage * self.adjustedTier)
		if (bodyGameObjectType == "ROCK"):
			body.takeDamage(MAIN.collisionDamage * self.adjustedTier)

