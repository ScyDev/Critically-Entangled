#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends "res://Projectile.gd"

var projectileWeaponType = "RAIL"

func _ready():
	self.set_max_contacts_reported(100)
	projectileSpeed = 300 # seems a good compromise. much faster and it will start going through objects. might still fall apart with fps drops?
	projectileDamage = 50
	
	self.adjustToTier()

func _on_Area_body_entered(collider):
	print("spawnedBy: ",spawnedBy, " collider: ",collider.name)
	if (collider != spawnedBy):
		print("collision")
		
		if (collider.get("gameObjectType") == "UNIT"):
			collider.takeDamage(self.projectileDamage)
		elif (collider.get("gameObjectType") == "ROCK"):
			collider.takeDamage(self.projectileDamage * MAIN.weaponDamageToRocksMultiplier)
			self.queue_free() # rail slugs don't penetrate rock, to allow units to use them as cover
		
		var newEffect = effectScene.instance()
		newEffect.set_translation(self.get_translation())
		get_node("/root/Main/Effects").add_child(newEffect)
		newEffect.playSound("rail_impact")
		
		# if rail slug hits a map object, destroy it
		if (collider.get("gameObjectType") == "GROUND"):
			self.queue_free()