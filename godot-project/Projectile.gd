#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends RigidBody

onready var MAIN = get_node("/root/Main")

var effectScene = preload("res://Effect.tscn")

var gameObjectType = "PROJECTILE"

var spawnedBy = null
var initialSpawnPosition = null

var adjustedTier = 1.0
var projectileSpeed = 0
var projectileDamage = 0

var timeToLive = 3.0
var maxRange = 0

var timer = null

func _ready():
	self.set_contact_monitor(true)
	self.set_max_contacts_reported(1)

func _physics_process(delta):
	if (self.maxRange > 0):
		if ((self.translation - self.initialSpawnPosition).length() > self.maxRange):
			self.queue_free()

func adjustToTier():
	for child in self.get_children():
		if (child.has_method("set_scale")):
			child.set_scale(Vector3(1, 1, 1) * self.adjustedTier)
	
	self.projectileDamage = self.projectileDamage * self.adjustedTier
	self.maxRange = self.maxRange * self.adjustedTier
	print("lol")

func startMoving(direction):
	self.initialSpawnPosition = self.translation
	
	self.apply_impulse(Vector3(0, 0, 0), direction * projectileSpeed)
	
	timer = Timer.new()
	timer.connect("timeout", self, "_on_timer_timeout")
	timer.wait_time = timeToLive
	timer.autostart = true
	add_child(timer)


func takeDamage(damage):
	self.queue_free()

func playSound(sample, loop):
	pass

func stopSound():
	pass

func _on_timer_timeout():
	#print("projectile timeout")
	self.queue_free()