shader_type spatial;

// player pos will be a world pos so we need our
// fragment position reported as world coordinate as well, using world_vertex_coords
//
// skip_vertex_transform: skip the conversion from model to view space
// world_vertex_coords: VERTEX/NORMAL/etc are modified in world coordinates instead of local. 
// 		+- fragment(): doesn't seem to change anything in this shader. with inverse(INV_CAMERA_MATRIX) below.
// 		+- vertex(): deforms the mesh in a circle, but moves with the camera. with inverse(INV_CAMERA_MATRIX) below.
render_mode unshaded;
// 		+- no render_mode specified (or unshaded): makes the mesh deform in a long wave but moves with the cam. with inverse(INV_CAMERA_MATRIX) below.

uniform float maxRange = 20;
uniform vec3 fogColor = vec3(0.0, 0.0, 0.0);
uniform float mapWidth;
uniform sampler2D unitPositions;


float calcFogBorder(vec2 uv) {
	float fogBorderWidth = 0.05;
	float gradient = 1.0;
	
	if (uv.x > (1.0 - fogBorderWidth)) {
		float minPos = 1.0 - fogBorderWidth;
		float posInBorder = uv.x - minPos;
		float newGradient = 1.0 - pow(posInBorder / fogBorderWidth, 5);
		//float newGradient = 1.0 - (fogBorderWidth / posInBorder); // Interesting! alpha > 1 makes a white glowing fog border. like blend_add, i think.
		if (newGradient < gradient) {
			gradient = newGradient;
		}
	}
	if (uv.x < fogBorderWidth) {
		float minPos = fogBorderWidth;
		float posInBorder = minPos - uv.x;
		float newGradient = 1.0 - pow(posInBorder / fogBorderWidth, 5);
		if (newGradient < gradient) {
			gradient = newGradient;
		}
	}
	if (uv.y > (1.0 - fogBorderWidth)) {
		float minPos = 1.0 - fogBorderWidth;
		float posInBorder = uv.y - minPos;
		float newGradient = 1.0 - pow(posInBorder / fogBorderWidth, 5);
		if (newGradient < gradient) {
			gradient = newGradient;
		}
	}
	if (uv.y < fogBorderWidth) {
		float minPos = fogBorderWidth;
		float posInBorder = minPos - uv.y;
		float newGradient = 1.0 - pow(posInBorder / fogBorderWidth, 5);
		if (newGradient < gradient) {
			gradient = newGradient;
		}
	}
	
	return gradient;
}

void vertex() {
	vec3 vertexTransformed = ((WORLD_MATRIX) * vec4(VERTEX, 1.0)).xyz;  // transform from view to world space
	
	float fx = vertexTransformed.x;
	float fy = vertexTransformed.y;
	float fz = vertexTransformed.z;
	
	ivec2 unitCount = textureSize(unitPositions, 0);
	float bitmapPixelWidth = 1.0 / float(unitCount.x); // offset used to read color from center of pixel
	bool fog = true;
	for (int i = 0; i < unitCount.x; i++) {
		float indexOnBitmap = 1.0 / float(unitCount.x) * float(i); // the pixel that holds current units position info
		vec4 bitmapPixel = texture(unitPositions, vec2((bitmapPixelWidth / 2.0) + indexOnBitmap, 0.5) ); // texture() reads positions on a bitmap between 0.0 and 1.0, not int pixels
		vec3 unitPos = vec3((bitmapPixel.x * mapWidth) - (mapWidth / 2.0), 0, (bitmapPixel.z * mapWidth) - (mapWidth / 2.0)); // transform back into world space
		
		float dr = sqrt((fx - unitPos.x) * (fx - unitPos.x) + (fz - unitPos.z) * (fz - unitPos.z)); // radius
		
		float innerRange = maxRange * 0.9;
		float adjustedMaxRange = maxRange * 1.0;
		float depth = 19.0;
		if (dr <= adjustedMaxRange) {
			if (dr <= innerRange) {
				VERTEX.y = -depth; // hard circular cutoff
			}
			else {
				// make the mesh fall off in a gradient. also helps a lot to make the circle appear round, not pixely, where it meets the ground.
				float dpct = 1.0 / (adjustedMaxRange - innerRange) * (dr - innerRange); // linear
				float newDepth = (1.0 - dpct) * depth; 
				if (VERTEX.y > -newDepth) {
					VERTEX.y = -newDepth;
				}
			}
			
//			float dpct = 1.0 - (pow(dr, 1.001) / adjustedMaxRange); // non-linear
//			float newHeight = dpct; 
//			VERTEX.y -= 36.0 * newHeight; // gravity-like, merging, attracting fields of fog dispersal around units. needs a very high value like 36 to fully disperse the fog with one unit. otherwise we need to make a pile of units to disperse it. cool accidential effect.
		} else {
			
		}
		
	}
}

void fragment() {
//	vec3 vertexTransformed = (inverse(INV_CAMERA_MATRIX) * vec4(VERTEX, 1.0)).xyz;  // transform from view to world space
//	ALBEDO = fogColor; 
//	ALPHA = 0.99;
//

	ALPHA = calcFogBorder(UV);

	vec3 vertexTransformed = (inverse(INV_CAMERA_MATRIX) * vec4(VERTEX, 1.0)).xyz;  // transform from view to world space

	float fx = vertexTransformed.x;
	float fy = vertexTransformed.y;
	float fz = vertexTransformed.z;

	ivec2 unitCount = textureSize(unitPositions, 0);
	float bitmapPixelWidth = 1.0 / float(unitCount.x); // offset used to read color from center of pixel
	bool fog = true;
	for (int i = 0; i < unitCount.x; i++) {
		float indexOnBitmap = 1.0 / float(unitCount.x) * float(i); // the pixel that holds current units position info
		vec4 bitmapPixel = texture(unitPositions, vec2((bitmapPixelWidth / 2.0) + indexOnBitmap, 0.5) ); // texture() reads positions on a bitmap between 0.0 and 1.0, not int pixels
		vec3 unitPos = vec3((bitmapPixel.x * mapWidth) - (mapWidth / 2.0), 0, (bitmapPixel.z * mapWidth) - (mapWidth / 2.0)); // transform back into world space

		float dr = sqrt((fx - unitPos.x) * (fx - unitPos.x) + (fz - unitPos.z) * (fz - unitPos.z)); // radius

		float innerRange = maxRange * 0.899;
		float adjustedMaxRange = maxRange * 0.93;
		if (dr <= adjustedMaxRange) {
			ALBEDO = fogColor; 
			if (dr > innerRange) {
				// calculate gradient
//				float dpct = 1.0 - (dr / maxRange); // linear
//				float dpct = 1.0 - (pow(dr, 1.001) / adjustedMaxRange); // non-linear
				float dpct = 1.0 / (adjustedMaxRange - innerRange) * (dr - innerRange); // linear only in a border of the circle
	
				float newAlpha = dpct;
				if (ALPHA > newAlpha) { // only make texture more transparent, never less. we don't want the gradient border when fogless areas overlap.
					ALPHA = newAlpha;
				}
			}
			else {
				ALPHA = 0.0
			}
		} else {
			ALBEDO = fogColor;
		}

	}

	// DEBUGGING:
	//ALBEDO = texture(unitPositions, UV).xyz; // maps the unitPositions texture onto the whole shader mesh
	// show unit count as grayscale block on edge of mesh
	if (UV.y > 0.99) {
		for (int i = 0; i < unitCount.x; i++) {
			float indexNormalized = 1.0 / float(unitCount.x) * float(i);
			if (UV.x > indexNormalized) {
				ALBEDO = vec3(indexNormalized, indexNormalized, indexNormalized);
			}
		}
	}
}
