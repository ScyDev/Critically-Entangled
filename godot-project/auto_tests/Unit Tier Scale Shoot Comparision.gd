extends Spatial

onready var MAIN = get_node("/root/Main")
onready var HUD = get_node("/root/Main/CameraMan/Camera/HUD")

var runTest = false
var testInitialized = false

var unitOffset = Vector3(0, 0, 0) #Vector3(-50, 0, -50)
var unitDistance = 20 

#onready var leftSideUnitType = MAIN.unitPlasmaScene
#onready var leftSideUnitType = MAIN.unitRailScene
#onready var leftSideUnitType = MAIN.unitSlingerScene
onready var leftSideUnitType = MAIN.unitLaserScene

var leftSide = []
var leftSideTier = 3 # Laser: 3 hits all 1 tier units, 4 misses all 1 tier units
#var leftSideTier = 5 # Plasma: currently hits tier 1 units from tier 18 and probably higher
var rightSide = []
var rightSideTier = 1

var testTickTimer = null

func _ready():
	if (MAIN.testingMode == true && self.runTest):
		self.testTickTimer = Timer.new()
		self.testTickTimer.connect("timeout", self, "_on_testTickTimer_timeout")
		self.testTickTimer.wait_time = 1.0
		self.add_child(self.testTickTimer)
		self.testTickTimer.start()

func _process(delta):
	if (self.runTest && MAIN.started == true && self.testInitialized == false):
		self.testInitialized = true
		self.initializeTest()

func initializeTest():
		MAIN.playersMainUnit.queue_free()
		for rock in MAIN.get_node("Ground/Obstacles").get_children():
			rock.queue_free()
		
		###############################################################
		# left side
		var laserT1_L1 = self.leftSideUnitType.instance()
		laserT1_L1.set_translation(self.unitOffset + Vector3(0, 0, 0 * self.unitDistance))
		MAIN.get_node("Units").add_child(laserT1_L1)
		laserT1_L1.adjustToTier(self.leftSideTier)
		leftSide.append(laserT1_L1)
		
		var laserT1_L2 = self.leftSideUnitType.instance()
		laserT1_L2.set_translation(self.unitOffset + Vector3(0, 0, 1 * self.unitDistance))
		MAIN.get_node("Units").add_child(laserT1_L2)
		laserT1_L2.adjustToTier(self.leftSideTier)
		leftSide.append(laserT1_L2)
		
		var laserT1_L3 = self.leftSideUnitType.instance()
		laserT1_L3.set_translation(self.unitOffset + Vector3(0, 0, 2 * self.unitDistance))
		MAIN.get_node("Units").add_child(laserT1_L3)
		laserT1_L3.adjustToTier(self.leftSideTier)
		leftSide.append(laserT1_L3)
		
		var laserT1_L4 = self.leftSideUnitType.instance()
		laserT1_L4.set_translation(self.unitOffset + Vector3(0, 0, 3 * self.unitDistance))
		MAIN.get_node("Units").add_child(laserT1_L4)
		laserT1_L4.adjustToTier(self.leftSideTier)
		leftSide.append(laserT1_L4)
		
		###############################################################
		# right side
		var laserT1_R1 = MAIN.unitLaserScene.instance()
		laserT1_R1.set_translation(self.unitOffset + Vector3(3 * self.unitDistance, 0, 0 * self.unitDistance))
		MAIN.get_node("Units").add_child(laserT1_R1)
		laserT1_R1.adjustToTier(self.rightSideTier)
		rightSide.append(laserT1_R1)
		
		var plasmaT1_R2 = MAIN.unitPlasmaScene.instance()
		plasmaT1_R2.set_translation(self.unitOffset + Vector3(3 * self.unitDistance, 0, 1 * self.unitDistance))
		MAIN.get_node("Units").add_child(plasmaT1_R2)
		plasmaT1_R2.adjustToTier(self.rightSideTier)
		rightSide.append(plasmaT1_R2)
		
		var railT1_R3 = MAIN.unitRailScene.instance()
		railT1_R3.set_translation(self.unitOffset + Vector3(3 * self.unitDistance, 0, 2 * self.unitDistance))
		MAIN.get_node("Units").add_child(railT1_R3)
		railT1_R3.adjustToTier(self.rightSideTier)
		rightSide.append(railT1_R3)
		
		var slingerT1_R4 = MAIN.unitSlingerScene.instance()
		slingerT1_R4.set_translation(self.unitOffset + Vector3(3 * self.unitDistance, 0, 3 * self.unitDistance))
		MAIN.get_node("Units").add_child(slingerT1_R4)
		slingerT1_R4.adjustToTier(self.rightSideTier)
		rightSide.append(slingerT1_R4)

func _on_testTickTimer_timeout():
	if (MAIN.started == true):
		for unit in self.leftSide:
			var targetPos = unit.get_translation() + Vector3(100, 0, 0)
			unit.prepareWeapon(unit.get_network_master(), targetPos)

		for unit in self.rightSide:
			var targetPos = unit.get_translation() + Vector3(-100, 0, 0)
			unit.prepareWeapon(unit.get_network_master(), targetPos)

		for unit in MAIN.get_node("Units").get_children():
			if (unit.health < (unit.maxHealth * 0.66)):
				unit.health = unit.maxHealth

