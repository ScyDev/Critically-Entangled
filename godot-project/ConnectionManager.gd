#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends Node

onready var LOBBY = get_node("/root/Main/CameraMan/Camera/Start/Lobby")

# should be the same number as maxPlayers below!
var availablePlayerColors = [
					{"color": Color("9a0808"), "used": false}, # blood red
					{"color": Color("6d0a50"), "used": false}, # purple
					{"color": Color("3d0a6d"), "used": false}, # violet
					{"color": Color("0c0c77"), "used": false}, # blue
					{"color": Color("1a8791"), "used": false}, # light blue
					{"color": Color("148324"), "used": false}, # green
					{"color": Color("183510"), "used": false}, # dark green
					{"color": Color("444444"), "used": false}  # gray
]

var host = null
const PORT = 1234
const maxPlayers = 8
var myPlayerName = null
var players = {}

func _ready():
	get_tree().connect("connected_to_server", self, "_connected_ok")

# starts a game
func on_host_game(playerName):
	self.myPlayerName = playerName
	host = NetworkedMultiplayerENet.new()
	host.create_server(self.PORT, self.maxPlayers) 
	get_tree().set_network_peer(host)
	
	_connected_ok()

# joins a game
func on_join_game(ip, playerName):
	self.myPlayerName = playerName
	host = NetworkedMultiplayerENet.new()
	host.create_client(ip, PORT)
	get_tree().set_network_peer(host)

# is run if this client (or server) itself is connected. NOT if another client connects to this server.
func _connected_ok():
	# register at server. the server on itself I guess. no other clients known yet.
	rpc("register_player", int(get_tree().get_network_unique_id()), self.myPlayerName) 
	
	# hide host/join panels
	LOBBY.get_node("Host").hide()
	LOBBY.get_node("Join").hide()
	LOBBY.get_node("Player").hide()

# this function is called when a new player is connected
sync func register_player(player_id, playerName):
	print("register player: ",player_id," ",playerName)
	
	# if I'm the server I inform the new connected player about the others
	if (get_tree().get_network_unique_id() == 1):
		randomize()
		
		# adding player to list
		# pick player color
		var colorIndex = randi() % self.availablePlayerColors.size()
		while (self.availablePlayerColors[colorIndex]["used"] == true):
			print("color ",colorIndex," already in use, pick another...")
			colorIndex = randi() % self.availablePlayerColors.size()
		self.availablePlayerColors[colorIndex]["used"] = true
		var playerColor = self.availablePlayerColors[colorIndex]["color"]
		var playerMaterial = SpatialMaterial.new()
		playerMaterial.albedo_color = playerColor
		
		# if player has no name, use his id
		if (playerName == null || playerName == "null" || playerName == ""):
			playerName = "Player "+str(player_id)
		
		players[player_id] = {
			"id": 		int(player_id), 
			"name":		playerName,
			"color": 	playerColor,
			"material":	playerMaterial,
			"tier":		0
			}
		
		# update players list on already connected clients
		rpc("updatePlayersList", players)
		
		LOBBY.get_node("PlayerList/ButtonStart").show()

# called everytime a new server joins on the player
sync func updatePlayersList(players):
	get_node("/root/Main").playersFromServer = players
	
	LOBBY.get_node("PlayerList/ItemList").clear()
	for player in players.values():
		LOBBY.get_node("PlayerList/ItemList").add_item(player.name)
