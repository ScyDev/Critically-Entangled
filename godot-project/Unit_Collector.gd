#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends "res://Unit.gd"

var unitType = "COLLECTOR"

var warningCooldownTimer = null
var warningInterval = 3.0

func _ready():
	self.maxHealth = 1000
	self.health = self.maxHealth
	
	# custom physics overrides for each specific unit
	self.unitMovementSpeed = 20
	self.set_angular_damp(0.999)
	self.set_linear_damp(0.99)
	
	self.warningCooldownTimer = Timer.new()
	self.warningCooldownTimer.wait_time = self.warningInterval
	self.warningCooldownTimer.set_one_shot(true)
	self.add_child(warningCooldownTimer)

func _physics_process(delta):
	pass

func prepareWeapon(player_id, targetPos = null):
	pass

sync func fireWeapon(player_id, mouseTargetPos):
	pass

func getHealth(amount):
	self.health += amount
	if (self.health > self.maxHealth):
		self.health = self.maxHealth
	
	rset_unreliable("slave_health", self.health)
	self.updateHealthBar()
	
	MAIN.playSoundPowerupHealth()
	
sync func takeDamage(damage):
	.takeDamage(damage)
	
	if (self.is_network_master()):
		if (self.warningCooldownTimer.time_left <= 0.0):
			if (self.health < (self.maxHealth * 0.2)):
				MAIN.get_node("Sound/Voice/CollectorAlmostDestroyed").play()
				self.warningCooldownTimer.start()
			elif (self.health < (self.maxHealth * 0.8)):
				MAIN.get_node("Sound/Voice/CollectorUnderAttack").play()
				self.warningCooldownTimer.start()

sync func destroyUnit():
	if (!self.destroyed):
		.destroyUnit() # it's important that this happens AFTER the yield() or it won't ever be reached because the unit is freed before the yield timer can elapse
		get_node("/root/Main").removePlayer(self.get_network_master())
