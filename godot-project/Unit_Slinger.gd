#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends "res://Unit.gd"

var unitType = "SLINGER"

func _ready():
	self.weaponType = WEAPON_TYPE_PROJECTILE
	self.projectileScene = preload("res://Projectile_Slinger.tscn")
	self.weaponName = "slinger"
	
	self.maxHealth = 300
	self.health = self.maxHealth
	
	# custom physics overrides for each specific unit
	self.unitMovementSpeed = 30
	self.set_angular_damp(0.999)
	self.set_linear_damp(0.99)
	
	self.projectileDirectionSpawnOffset = 0
	self.projectileSpawnOffset = Vector3(0, 0, 0)
	
	self.weaponCooldown = 2.0
	self.weaponMinRange = 20
	self.weaponMaxRange = 150
	
	self.adjustToTier(self.tier)

func _physics_process(delta):
	pass

func prepareWeapon(player_id, targetPos = null):
	if (self.weaponReady == true):
		# this should be != null only during test
		var mouseTargetPos = targetPos
		if (mouseTargetPos == null):
			mouseTargetPos = MAIN.getMouseTargetPoint()
		
		if ((mouseTargetPos != null)
			&& (mouseTargetPos - self.translation).length() <= self.weaponMaxRange
			&& (mouseTargetPos - self.translation).length() >= self.weaponMinRange):
				self.weaponReady = false
				self.startWeaponCooldownTimer()
				
				self.rpc("fireWeapon", player_id, mouseTargetPos)
#		else:
#			MAIN.playSoundDenyClick()
#	else:
#		MAIN.playSoundDenyClick()

sync func fireWeapon(player_id, mouseTargetPos):
	# point unit towards target
#	self.destinationPoint = mouseTargetPos
	self.setDestinationPoint(self.translation)
	self.lookAtTarget(mouseTargetPos)
	
	var newProjectile = projectileScene.instance()
	newProjectile.set_network_master(player_id)
	newProjectile.spawnedBy = self
	newProjectile.maxRange =  self.weaponMaxRange
	newProjectile.adjustedTier = self.adjustedTier
	
	var direction = mouseTargetPos - self.translation
	var launchDirection = direction
	# adjust y so the vector will point upwards at close to an 45° angle, to match with the formula
	if abs(launchDirection.x) >= abs(launchDirection.z):
		launchDirection.y = abs(launchDirection.x)
	else:
		launchDirection.y = abs(launchDirection.z)
	launchDirection = launchDirection.normalized()
	
	var projectileSpawnPos = self.translation + (direction.normalized() * projectileDirectionSpawnOffset * self.adjustedTier) + (projectileSpawnOffset * self.adjustedTier)
	var actualDistance = (mouseTargetPos - projectileSpawnPos).length()
#	print("actualDistance to lob target ", actualDistance)
	
	# calculate trajectory for lob projectile to hit at mouse pointer
	var g = 9.81 # gravity
	var x = actualDistance # target x
	var y = 0 # target y # value > 0 leads to division by 0 in the formula below...
	var o = 42 # launch angle # angles around 43 also lead to division by 0. wtf...
	var v = (sqrt(g) * sqrt(x) * sqrt((tan(o)*tan(o))+1)) / sqrt(2 * tan(o) - (2 * g * y) / x) # velocity
#	print("projectile lob velocity ", v)
	v = v - (v/7) # the formula above (with launch angle 42) shoots a bit too far, but pretty consistently. adjust proportionally to distance. the value is found by trial and error.
#	print("corrrected lob velocity ", v)
	
	newProjectile.rotation = self.rotation
	newProjectile.translation = projectileSpawnPos
	#newProjectile.get_node("CollisionShape").disabled = true
	get_node("/root/Main/Projectiles").add_child(newProjectile)
	newProjectile.projectileSpeed = v
	newProjectile.startMoving(launchDirection)
	
	self.playSound(self.weaponName+"_fire")



