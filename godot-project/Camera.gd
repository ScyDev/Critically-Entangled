#
#    This file is part of Critically Entangled.
#    Copyright (c) 2018 Lukas Sägesser, ScyDev GmbH, Switzerland.
#
#    Critically Entangled is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Critically Entangled is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Critically Entangled.  If not, see <http://www.gnu.org/licenses/>.
#

extends Camera

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	pass

func _physics_process(delta):
#	if Input.is_action_just_released("ui_zoom_out"):
#		var direction = self.get_global_transform().basis.y
#		self.get_parent().apply_impulse(Vector3(0, 0, 0), Vector3(0, 1, 0) * zoomSpeed * delta)
#	if Input.is_action_just_released("ui_zoom_in"):
#		var direction = self.get_global_transform().basis.y
#		self.get_parent().apply_impulse(Vector3(0, 0, 0), Vector3(0, -1, 0) * zoomSpeed * delta)
	pass

